import React from "react";
import { connect } from "react-redux";
import { toggleTodo, toggleEditingStatus, handleTodoInput } from "../actions";

const Todo = ({
  editingStatus,
  todo,
  toggleTodo,
  toggleEditingStatus,
  handleTodoInput
}) => {
  return (
    <div className={"d-flex list-group-item align-items-center"}>
      <input
        className="mr-3"
        type="checkbox"
        checked={todo.selected}
        onChange={() => toggleTodo(todo.id)}
        disabled={editingStatus ? "disabled" : ""}
      />
      <div
        className="w-100 todo"
        onClick={() => {
          if (!editingStatus) handleTodoInput(todo);
        }}
      >
        {todo.text}
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  todos: state.todos,
  editingStatus: !!state.editingStatus
});

const mapDispatchToProps = dispatch => ({
  toggleTodo: id => {
    dispatch(toggleTodo(id));
  },
  handleTodoInput: todo => {
    dispatch(toggleEditingStatus(todo.id));
    dispatch(handleTodoInput(todo.text));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Todo);
