import { combineReducers } from "redux";
import todos from "./todos";
import todoInput from "./todoInput";
import editingStatus from "./editingStatus";

const reducer = combineReducers({
  todos,
  editingStatus,
  todoInput
});

export default reducer;
