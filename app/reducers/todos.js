const todos = (state = [], action) => {
  switch (action.type) {
    case "ADD_TODO":
      return [{ id: action.id, text: action.text, selected: false }, ...state];
    case "TOGGLE_TODO":
      return state.map(todo => {
        if (todo.id === action.id) {
          return { ...todo, ...{ selected: !todo.selected } };
        }
        return todo;
      });
    case "DELETE_TODOS":
      return state.filter(todo => {
        if (!todo.selected) {
          return todo;
        }
      });
    case "UPDATE_TODO":
      return state.map(todo => {
        if (todo.id === action.id) {
          todo.text = action.text;
        }
        return todo;
      });

    default:
      return state;
  }
};

export default todos;
