const todoInput = (state = "", action) => {
  if (action.type === "HANDLE_TODO_INPUT") return action.text;
  return state;
};

export default todoInput;
