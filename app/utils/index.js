import { createStore } from "redux";
import reducer from "../reducers";

const loadState = () => {
    try {
        const state = localStorage.getItem("state");
        if (!state) return undefined;
        return JSON.parse(state);
    } catch (err) {
        return undefined;
    }
};

const saveState = state => {
    try {
        localStorage.setItem("state", JSON.stringify(state));
    } catch (err) {
        console.log("Failed to store in local storage");
    }
};

const configureStore = () => {
    const store = createStore(
        reducer,
        loadState(),
        window.__REDUX_DEVTOOLS_EXTENSION__ &&
            window.__REDUX_DEVTOOLS_EXTENSION__()
    );

    store.subscribe(() => {
        saveState({
            todos: store.getState().todos
        });
    });
    return store;
};

export default configureStore();