import React from "react";
import { connect } from "react-redux";

import Header from "../components/Header.js";
import AddTodo from "../containers/AddTodo";
import TodosList from "../containers/TodosList";
import { deleteTodos } from "../actions";

const App = ({ editingStatus, todoSelected, deleteTodos }) => (
  <div className="container">
    <Header />
    <div className="box">
      <AddTodo />
      <div>
        {todoSelected && (
          <button
            type="submit"
            className={`btn btn-outline-danger ml-3 ${editingStatus
              ? "disabled"
              : ""}`}
            onClick={() => {
              if (!editingStatus) deleteTodos();
            }}
          >
            DELETE
          </button>
        )}
        <div className="list-group">
          <TodosList />
        </div>
      </div>
    </div>
  </div>
);

const mapStateToProps = state => ({
  todoSelected: state.todos.some(task => task.selected),
  editingStatus: !!state.editingStatus
});

const mapDispatchToProps = dispatch => ({
  deleteTodos: () => {
    dispatch(deleteTodos());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
