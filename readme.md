# Todos App using React and Redux

This is a basic application where we can add, view, and delete todos.

#### How to use
- Click on a Todo to update. 
- Hit `ESC` to cancel. 
- Select one or more todos to `DELETE`. 

#### For Testing and Development
- `clone the repo`
- `npm install`
- `npm start` will start the development server on `localhost:8080`