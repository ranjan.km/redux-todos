import v4 from "uuid/v4";

export function addTodo(todo) {
  return {
    type: "ADD_TODO",
    id: v4(),
    text: todo
  };
}

export function toggleTodo(id) {
  return {
    type: "TOGGLE_TODO",
    id
  };
}

export function deleteTodos() {
  return {
    type: "DELETE_TODOS"
  };
}

export function toggleEditingStatus(id) {
  return {
    type: "TOGGLE_EDITING_STATUS",
    id
  };
}

export function updateTodo(id, text) {
  return {
    type: "UPDATE_TODO",
    id,
    text
  };
}

export function handleTodoInput(text) {
  return {
    type: "HANDLE_TODO_INPUT",
    text
  };
}
