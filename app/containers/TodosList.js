import React from "react";
import Todo from "./Todo";
import { toggleTodo } from "../actions/index";
import { connect } from "react-redux";

const TodosList = ({ todos, toggleTodo }) => (
  <div className="mt-3 list-group">
    {todos.map(todo => (
      <Todo key={todo.id} todo={todo} toggleTodo={toggleTodo} />
    ))}
  </div>
);

const mapStateToProps = state => ({
  todos: state.todos
});

const mapDispatchToProps = dispatch => ({
  toggleTodo: id => {
    dispatch(toggleTodo(id));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(TodosList);
