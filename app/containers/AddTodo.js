import React from "react";
import { connect } from "react-redux";
import {
  addTodo,
  updateTodo,
  toggleEditingStatus,
  handleTodoInput
} from "../actions";
import { Form } from "formsy-react";
import TodoInput from "../components/TodoInput.js";

const AddTodo = ({
  addTodo,
  editingStatus,
  updateTodo,
  cancelEdit,
  handleTodoInput,
  todoInput
}) => {
  return (
    <Formsy.Form
      className="d-flex form-group"
      onSubmit={input => {
        if (input.todo !== "") {
          if (!editingStatus) {
            addTodo(input.todo);
          } else {
            updateTodo(editingStatus, input.todo);
          }
        }
      }}
      onKeyUp={key => {
        if (key.keyCode === 27) {
          cancelEdit();
        }
      }}
    >
      <TodoInput
        className="form-control font-weight-light font-italic"
        name="todo"
        value={todoInput}
        handleTodoInput={handleTodoInput}
        required
      />
      <button
        type="submit"
        className="btn btn-outline-success ml-3"
        disabled={!todoInput}
      >
        {editingStatus ? "EDIT" : "SUBMIT"}
      </button>
    </Formsy.Form>
  );
};

const mapStateToProps = state => ({
  editingStatus: state.editingStatus,
  todoInput: state.todoInput
});

const mapDispatchToProps = dispatch => ({
  addTodo: text => {
    dispatch(addTodo(text));
    dispatch(toggleEditingStatus(null));
    dispatch(handleTodoInput(""));
  },
  updateTodo: (id, text) => {
    dispatch(updateTodo(id, text));
    dispatch(toggleEditingStatus(null));
    dispatch(handleTodoInput(""));
  },
  cancelEdit: () => {
    dispatch(toggleEditingStatus(null));
    dispatch(handleTodoInput(""));
  },
  handleTodoInput: text => {
    dispatch(handleTodoInput(text));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTodo);
