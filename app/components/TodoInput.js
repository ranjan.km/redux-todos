import React from "react";
import Formsy from "formsy-react";
import createReactClass from "create-react-class";

const TodoInput = createReactClass({
  mixins: [Formsy.Mixin],
  changeValue(event) {
    this.props.handleTodoInput(event.currentTarget["value"]);
  },
  render() {
    const className =
      (this.props.className || " ") +
      " " +
      (this.showRequired() ? "required" : null);
    return (
      <input
        className={className}
        type="text"
        name={this.props.name}
        onChange={this.changeValue}
        placeholder="What needs to be done?"
        value={this.props.value}
      />
    );
  }
});

export default TodoInput;
