import React from "react";
import ReactDOM from "react-dom";

import Root from "./components/Root.js";
import store from "./utils";
import "bootstrap/dist/css/bootstrap.min.css";
require("./index.css");

ReactDOM.render(<Root store={store} />, document.getElementById("app"));
