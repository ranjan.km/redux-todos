const editingStatus = (state = null, action) => {
  switch (action.type) {
    case "TOGGLE_EDITING_STATUS":
      return action.id;
    default:
      return state;
  }
};

export default editingStatus;
